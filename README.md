# Eval



## Presentation projet

Le projet consiste a deploié Terraform automatiquement en mettant 
en place dans le cloud une application Scaleway utilisant des instances et une base de donnée (mariadb) sur l'Internet.


## Description de l'architecture 

Notre projet est construit avec un "load balancer" qui est accessible avec une Ip publique qu'on récupper à l'aide d'une commande et qui permet de donner l' accès aux deux instances wordpress (web1, web2).
Ces dernières ont toutes les deux une interface web, qui récupérent toutes les deux une base de données (mariadb)

![MicrosoftTeams-image](/uploads/328fe2342d35fcf925e47f35fdd60e69/MicrosoftTeams-image.png)

## Comment récupérer les fichiers du projet
```
apt install git
git clone https://gitlab.com/paul.oussoultzoglou/eval.git
cd eval/Tf
echo "coin-coin" > teamname.txt
```

## Deploiment de la base de donnée et les instances

```
make init
make apply
./go mariadb " #Connection à la base de donnée
watch pstree #pour voir les processus en cours le temps que le bastion soit accessible 
```

## Connection aux instances
on peut se connecter aux instances avec les commandes ci-dessous : 

```
./go web1
#./go web2
```
on peut vérifier que l'interface web est accessible avec une commande
en récupérant l'adresse ip du bastion
```
terraform output  # récupperation du lb_ip
curl -I http://(l'adresse ip du bastion(lb_ip)) # vérification de la disponibiltes de l'interface web
```
Cette dernière commande nous retourne code qui correspond à la réussite de la connexion internet

![curl](/uploads/d09dc1f75bc69c205221067b50e465a7/curl.png)

on peut aussi vérifier sur un navigateur web si l'interface web est présente



## Structure du script


# Instances.Tf 
 ## Définition :
    Une instance dans le cloud computing est une ressource serveur fournie par des services cloud tiers. Bien que vous puissiez gérer et maintenir les ressources des serveurs physiques sur site, cela est coûteux et inefficace. Les fournisseurs de cloud entretiennent le matériel dans leurs centres de données et vous donnent un accès virtuel aux ressources de calcul sous la forme d'une instance. Vous pouvez utiliser l'instance cloud pour exécuter des charges de travail intensives en calcul comme les conteneurs, les bases de données, les microservices et les machines virtuelles

On donne tous les informations pour créer les instances scaleway avec les paramètres demandé et interface web,
Géneration de l'ip Vpc de l'instance, les deux instances sont aussi placé dans le même sécurity group

# Lb.Tf
 ## Définition 
    En informatique, la répartition de charge (en anglais : load balancing) désigne le processus de répartition d’un ensemble de tâches sur un ensemble de ressources, dans le but d’en rendre le traitement global plus efficace. Les techniques de répartition de charge permettent à la fois d’optimiser le temps de réponse pour chaque tâche, tout en évitant de surcharger de manière inégale les nœuds de calcul.

Génération du loadBalancer : 
    récuperation de l'ip du LoadBalancer
    mise en place de lb dans le vpc
    Gestion de l'ip des deux instances

# provider.Tf 
 ## Définition
    Un fournisseur d'accès à Internet ou FAI est un organisme offrant une connexion à Internet, le réseau informatique mondial

Gestion du fournisseur d'accès aux instances, provenance de l'instance et non du projet

 # security.Tf
  ## Définition 
    La sécurité cloud, également connue sous le nom de sécurité de l'informatique infonuagique, consiste en un ensemble de politiques, de contrôles, de procédures et de technologies qui fonctionnent ensemble pour protéger les systèmes, les données et l'infrastructure basés sur le cloud.
 Création du sécurity group avec les paramètres nécessaire pour se connecté aux instances

 #vpc.Tf
  ## Définition 
    Virtual Private Cloud (VPC) Un réseau virtuel mondial couvrant toutes les régions. Un VPC unique pour l'ensemble d'une organisation, permettant d'isoler les charges de travail dans des projets.

 Génération du vpc privé et publique et activation du bastion, gestion de la pool de génération de l'adresse Ip
 puis affiche l'ip publique du bastion





