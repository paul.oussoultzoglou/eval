#resource "scaleway_instance_ip" "public_ip1" {
#  project_id = var.project_id
#}

#resource "scaleway_instance_ip" "public_ip2" {
#  project_id = var.project_id
#}

resource "scaleway_instance_server" "web1" {
  name = "${local.team}-web1"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"]

 # ip_id = scaleway_instance_ip.public_ip1.id

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 10
  }

  user_data = {
    name        = "web1"
    myip        = "10.42.42.11"
    cloud-init = file("${path.module}/deploy-web")
    # "web1ip"   = "${scaleway_instance_server.web1.public_ip}"
  }



  security_group_id = scaleway_instance_security_group.sg-www.id

  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }


}

resource "scaleway_instance_server" "web2" {
  name = "${local.team}-web2"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"]
  
  user_data = {
    name        = "web2"
    myip        = "10.42.42.12"
    cloud-init = file("${path.module}/deploy-web")
    # "web1ip"   = "${scaleway_instance_server.web1.public_ip}"
  }

  #depends_on =  [ scaleway_instance_server.web1 ]
  #ip_id = scaleway_instance_ip.public_ip2.id

  #additional_volume_ids = [scaleway_instance_volume.data.id]

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 10
  }

  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }

  security_group_id = scaleway_instance_security_group.sg-www.id
}


resource "scaleway_instance_server" "MariaDB" {
  name = "${local.team}-MariaDB"

  project_id = var.project_id
  type       = "PRO2-XXS"
  image      = "debian_bullseye"

  tags = ["front", "web"] # see later

 # ip_id = scaleway_instance_ip.public_ip3.id

  root_volume {
    # The local storage of a DEV1-L instance is 80 GB, subtract 30 GB from the additional l_ssd volume, then the root volume needs to be 50 GB.
    size_in_gb = 10
  }

  user_data = {
    name        = "MariaDB"
    myip        = "10.42.42.99"
    cloud-init = file("${path.module}/deploy-db")
    # "web1ip"   = "${scaleway_instance_server.web1.public_ip}"
  }



  security_group_id = scaleway_instance_security_group.sg-www.id

  private_network {
    pn_id = scaleway_vpc_private_network.myvpc.id
  }
}


#output "web1_ip" {
#  value = "${scaleway_instance_server.web1.public_ip}"
#}

#output "web2_ip" {
#  value = "${scaleway_instance_server.web2.public_ip}"
#}

#output "MariaDB_ip" {
#  value = "${scaleway_instance_server.MariaDB.public_ip}"
#}

